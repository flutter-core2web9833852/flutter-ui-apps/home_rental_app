import 'package:flutter/material.dart';
import 'package:home_rental_app/HomePage.dart';

class Startpage extends StatefulWidget {
  const Startpage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _StartPageState();
  }
}

class _StartPageState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(237, 237, 237, 1),
      body: Column(
        children: [
          Image.asset(
            "assets/startImg.png",
            scale: 0.9,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 45.0, right: 45),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const Text(
                    "Lets find your Paradise",
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.w800),
                  ),
                  const Text(
                    "Find your perfect dream spacewith just a few clicks", //keep in center
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: Color.fromARGB(255, 56, 56, 56)),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Homepage()));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(30)),
                      child: const Padding(
                        padding: EdgeInsets.only(
                            left: 50, right: 50, top: 12, bottom: 12),
                        child: Column(
                          children: [
                            Text(
                              "Get Started",
                              style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
